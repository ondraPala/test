-- TRIDA zamestnanec_obj
--
CREATE TYPE zamestnanec_obj AS OBJECT(
  idZamestnance NUMBER,
  jmeno VARCHAR2(30),
  prijmeni VARCHAR2(30),
  MEMBER PROCEDURE vykazPraci(p_pocetHodin NUMBER, 
    p_poznamka VARCHAR2, 
    p_idUlohy NUMBER,
    p_datum DATE),
  MEMBER PROCEDURE zobrazVykazPrace(p_mesic NUMBER, 
    p_rok NUMBER),
  MEMBER PROCEDURE upravVykazPrace(p_idVykazu NUMBER, 
    p_idulohy NUMBER, 
    p_datum DATE,
    p_pocetHodin NUMBER,
    p_poznamka VARCHAR2),
  MEMBER PROCEDURE smazVykazPrace(p_idVykazu NUMBER)
);

CREATE OR REPLACE TYPE BODY zamestnanec_obj AS
  MEMBER PROCEDURE vykazPraci(p_pocetHodin NUMBER,
    p_poznamka VARCHAR2, 
    p_idUlohy NUMBER,
    p_datum DATE) AS
      vykaz VYKAZ_OBJ;
      error EXCEPTION;
      PRAGMA EXCEPTION_INIT(error, -20001);
      test NUMBER;
    BEGIN
      -- kontrola existence dane ulohy
      SELECT 1 INTO test FROM ulohy WHERE idUlohy = p_idUlohy;
      --
      -- pokud je p_datum NULL - je pouzito aktualni datum
      IF p_datum IS NOT NULL THEN
        vykaz := VYKAZ_OBJ(vykazy_seq.NEXTVAL, self.idZamestnance, p_idUlohy, p_datum, p_pocetHodin, p_poznamka);
      ELSE
        vykaz := VYKAZ_OBJ(vykazy_seq.NEXTVAL, self.idZamestnance, p_idUlohy, SYSDATE, p_pocetHodin, p_poznamka); 
      END IF;
      --
      INSERT INTO vykazy VALUES (vykaz);
      --
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          RAISE_APPLICATION_ERROR(-20001, 'Zvolená úloha neexistuje');
      --
    END vykazPraci;
  --
  MEMBER PROCEDURE zobrazVykazPrace(p_mesic NUMBER,
    p_rok NUMBER) AS
    TYPE t_vykazy_rec IS RECORD(
        nazev ulohy.nazev%TYPE,
        pocetHodin vykazy.pocetHodin%TYPE,
        poznamka vykazy.poznamka%TYPE
      );
    TYPE t_vykazy_tab IS TABLE OF T_VYKAZY_REC INDEX BY PLS_INTEGER;
    vykazy_tab T_VYKAZY_TAB;
    suma PLS_INTEGER := 0;
    BEGIN
      SELECT 
        u.nazev, 
        v.pocethodin, 
        v.poznamka
        BULK COLLECT INTO vykazy_tab 
      FROM 
        vykazy v, ulohy u
      WHERE 
        v.idulohy = u.idulohy AND
        v.idZamestnance = self.idZamestnance AND 
        EXTRACT(YEAR FROM v.datum) = p_rok AND
        EXTRACT(MONTH FROM v.datum) = p_mesic
      ORDER BY
        datum;
      --
      DBMS_OUTPUT.PUT_LINE('Vykaz: ' || self.jmeno || ' ' || self.prijmeni || 
        ' za měsíc: ' || p_mesic || '/' || p_rok);
      --
      FOR l_vykaz IN 1 .. vykazy_tab.COUNT LOOP
        DBMS_OUTPUT.PUT_LINE(vykazy_tab(l_vykaz).nazev || ' | ' || 
          vykazy_tab(l_vykaz).pocethodin || ' | ' || 
          vykazy_tab(l_vykaz).poznamka);
        suma := suma + vykazy_tab(l_vykaz).pocethodin;
      END LOOP;
      --
      DBMS_OUTPUT.PUT_LINE('Celkem hodin: ' || suma);
      --
  END zobrazVykazPrace;
  --
  MEMBER PROCEDURE upravVykazPrace(p_idVykazu NUMBER, 
    p_idulohy NUMBER, 
    p_datum DATE,
    p_pocetHodin NUMBER,
    p_poznamka VARCHAR2) AS
    test NUMBER;
    error EXCEPTION;
    PRAGMA EXCEPTION_INIT(error, -20001);
    vykaz VYKAZ_OBJ;
  BEGIN
    -- kontrola existence zadaneho vykazu + kontrola existence zadaneho ulohy
    SELECT 1 INTO test FROM vykazy v, ulohy u WHERE v.idVykazu = p_idVykazu AND p_idulohy = u.idulohy;
    --
    SELECT VALUE(v) INTO vykaz FROM vykazy v WHERE v.idVykazu = p_idVykazu;
    -- kontrola zadanych parametru
    vykaz.idulohy := p_idulohy;
    --
    IF (p_datum IS NOT NULL) THEN
      vykaz.datum := p_datum;
    END IF;
    --
    IF (p_pocetHodin IS NOT NULL) THEN
      vykaz.pocethodin := p_pocetHodin;
    END IF;
    --
    IF (p_poznamka IS NOT NULL) THEN
      vykaz.poznamka := p_poznamka;
    END IF;
    --
    UPDATE vykazy v
      SET VALUE(v) = vykaz
      WHERE v.idVykazu = p_idVykazu;
    --
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20001, 'Zvolený výkaz nebo úloha neexistuje');
  END upravVykazPrace;
  --
  MEMBER PROCEDURE smazVykazPrace(p_idVykazu NUMBER) AS
    test NUMBER;
    error EXCEPTION;
    PRAGMA EXCEPTION_INIT(error, -20001);
  BEGIN
    -- kontrola existence vykazu
    SELECT 1 INTO test FROM vykazy WHERE idVykazu = p_idVykazu;
    --
    DELETE FROM vykazy WHERE idVykazu = p_idVykazu;
    --
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20001, 'Zvolený výkaz neexistuje');  
  END smazVykazPrace;
END;
--
CREATE TABLE zamestnanci OF zamestnanec_obj(
  idZamestnance CONSTRAINT pk_zamestnanci PRIMARY KEY
);
--
CREATE SEQUENCE zamestnanci_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
--
-- TRIDA vykaz_obj
--
CREATE TYPE vykaz_obj AS OBJECT(
  idVykazu NUMBER,
  idZamestnance NUMBER,
  idUlohy NUMBER,
  datum DATE,
  pocetHodin NUMBER,
  poznamka VARCHAR2(100)
);
--
CREATE TABLE vykazy OF vykaz_obj(
  idVykazu CONSTRAINT pk_vykazy PRIMARY KEY,
  idZamestnance CONSTRAINT fk_zamestnanci REFERENCES zamestnanci(idZamestnance) ON DELETE CASCADE,
  idUlohy CONSTRAINT fk_ulohy REFERENCES ulohy(idUlohy) ON DELETE CASCADE
);
--
CREATE SEQUENCE vykazy_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
--
-- TRIDA uloha_obj
--
CREATE TYPE uloha_obj AS OBJECT(
  idUlohy NUMBER,
  idProjektu NUMBER,
  nazev VARCHAR2(30),
  popis VARCHAR2(70)
);
--
CREATE TABLE ulohy OF uloha_obj(
  idUlohy CONSTRAINT pk_ulohy PRIMARY KEY,
  idProjektu CONSTRAINT fk_projekty REFERENCES projekty(idProjektu) ON DELETE CASCADE
);
--
CREATE SEQUENCE ulohy_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
--
-- TRIDA projekt_obj
--
CREATE TYPE projekt_obj AS OBJECT(
  idProjektu NUMBER,
  nazev VARCHAR2(30),
  popis VARCHAR2(70)
);
--
CREATE TABLE projekty OF projekt_obj(
  idProjektu CONSTRAINT pk_projekty PRIMARY KEY
);
--
CREATE SEQUENCE projekty_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
--
--
-- Testovací data
INSERT INTO projekty
VALUES(projekt_obj(projekty_seq.NEXTVAL, 'Banka - platební karty', 'Implementace modulu pro práci s platebními kartami.'));
--
INSERT INTO projekty
VALUES(projekt_obj(projekty_seq.NEXTVAL, 'Banka - bankomat', 'Implementace modulu pro práci s bankomaty.'));
--
INSERT INTO projekty
VALUES(projekt_obj(projekty_seq.NEXTVAL, 'Webová aplikace - AHOJ', 'Implementace webové aplikace pro zdravení.'));
--
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 1, 'Jednání se zákazníkem', 'Služební cesta a jednání se zákazníkem.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 1, 'Analýza', 'Analýza požadavků zákazníka a tvorba zadání.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 1, 'Implementace', 'Vývoj daného systému.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 1, 'Testování', 'Testování naimplementovaného systému.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 1, 'Předání', 'Předání systému zákazníkovi a jeho zaškolení.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 2, 'Jednání se zákazníkem', 'Služební cesta a jednání se zákazníkem.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 2, 'Analýza', 'Analýza požadavků zákazníka a tvorba zadání.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 2, 'Implementace', 'Vývoj daného systému.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 2, 'Testování', 'Testování naimplementovaného systému.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 2, 'Předání', 'Předání systému zákazníkovi a jeho zaškolení.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 3, 'Jednání se zákazníkem', 'Služební cesta a jednání se zákazníkem.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 3, 'Analýza', 'Analýza požadavků zákazníka a tvorba zadání.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 3, 'Implementace', 'Vývoj daného systému.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 3, 'Testování', 'Testování naimplementovaného systému.'));
--
INSERT INTO ulohy
VALUES(uloha_obj(ulohy_seq.NEXTVAL, 3, 'Předání', 'Předání systému zákazníkovi a jeho zaškolení.'));
--
INSERT INTO zamestnanci
VALUES(zamestnanec_obj(zamestnanci_seq.NEXTVAL, 'Jan', 'Novák'));
--
INSERT INTO zamestnanci
VALUES(zamestnanec_obj(zamestnanci_seq.NEXTVAL, 'Jana', 'Nováková'));
--
INSERT INTO zamestnanci
VALUES(zamestnanec_obj(zamestnanci_seq.NEXTVAL, 'Josef', 'Oracle'));
--
INSERT INTO zamestnanci
VALUES(zamestnanec_obj(zamestnanci_seq.NEXTVAL, 'Jiří', 'Tichý'));